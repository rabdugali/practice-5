package aitu.java;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Necklace {
    private double total_weight;
    private double total_price;
    private double chain_weight = 344;
    private PreciousStones prS;
    private double prWeight;
    private SemipreciousStones semiprS;
    private double semiprWeight;

    public double getTotal_weight() {
        return this.total_weight;
    }

    public double getTotal_price() {
        return this.total_price;
    }

    public void addPreciousStones(PreciousStones stone, double weight){
        this.prS = stone;
        this.prWeight = weight;
    }
    public void addSemipreciousStones(SemipreciousStones stone, double weight){
        this.semiprS = stone;
        this.semiprWeight = weight;
    }
    public void calculateWeight(){
        this.total_weight = this.prWeight + this.semiprWeight + chain_weight;
    }
    public void calculatePrice(){
        this.total_price = ((this.prS.getPrice_per_carat() * this.prWeight) + (this.semiprS.getPrice_per_carat() * this.semiprWeight) + 5500) * 427; // 5500$ for the chain and for work
    }
    public void buyNecklace(Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        double w1 = this.prS.getWeight() - this.prWeight;
        double w2 = this.semiprS.getWeight() - this.semiprWeight;
        stmt.executeUpdate("update stones set weight = " + w1 + " where stone_id = " + prS.getId());
        stmt.executeUpdate("update stones set weight = " + w2 + " where stone_id = " + semiprS.getId());
        System.out.println("You successfully both a necklace!!!");
    }
}

package aitu.java;

import java.sql.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException {
        Scanner sc = new Scanner(System.in);
        DB connection = new DB();
        connection.setConnection();
        if (connection.getConnection() != null) {
            ArrayList<PreciousStones> preciousStones = new ArrayList();
            ArrayList<SemipreciousStones> semipreciousStones = new ArrayList();
            Necklace necklace = new Necklace();
            Statement stmt = connection.getConnection().createStatement();
            ResultSet resultSet = stmt.executeQuery("select * from stones where type = 'precious'");
            System.out.println("Choose precious stones for your necklace(to add stone write the ID of the stones in the console):");
            while (resultSet.next()) {
                PreciousStones preciousStone = new PreciousStones(resultSet.getInt("stone_id"), resultSet.getString("name"), resultSet.getDouble("price_per_carat"), resultSet.getDouble("weight"));
                preciousStones.add(preciousStone);
                System.out.println(resultSet.getString("stone_id") + "\t" + resultSet.getString("name"));
            }
            int ch = sc.nextInt();
            double carat;
            for(int i = 0; i < preciousStones.size(); i++){
                if (preciousStones.get(i).getId() == ch) {
                    System.out.println("How many carat you want(max " + preciousStones.get(i).getWeight() + "):");
                    carat = sc.nextDouble();
                    necklace.addPreciousStones(preciousStones.get(i), carat);
                    break;
                }
            }
            resultSet = stmt.executeQuery("select * from stones where type = 'semiprecious'");
            System.out.println("Choose semiprecious stones for your necklace(to add stone write the ID of the stones in the console):");
            while (resultSet.next()) {
                SemipreciousStones semipreciousStone = new SemipreciousStones(resultSet.getInt("stone_id"), resultSet.getString("name"), resultSet.getDouble("price_per_carat"), resultSet.getDouble("weight"));
                semipreciousStones.add(semipreciousStone);
                System.out.println(resultSet.getString("stone_id") + "\t" +resultSet.getString("name"));
            }
            ch = sc.nextInt();
            for(int i = 0; i < semipreciousStones.size(); i++){
                if (semipreciousStones.get(i).getId() == ch) {
                    System.out.println("How many carat you want(max " + semipreciousStones.get(i).getWeight() + "):");
                    carat = sc.nextDouble();
                    necklace.addSemipreciousStones(semipreciousStones.get(i), carat);
                    break;
                }
            }
            necklace.calculateWeight();
            System.out.println("Total weight: " + necklace.getTotal_weight());
            necklace.calculatePrice();
            System.out.println("Total price(tg): " + necklace.getTotal_price());
            System.out.println("Do you want to buy a necklace?(yes/no)");
            String choice = sc.next();
            if (Objects.equals(choice, "yes")){
                necklace.buyNecklace(connection.getConnection());
            } else {
                System.out.println("Good bye");
            }
            connection.closeConnection();
        } else {
            System.out.println("Failed to make connection!");
        }
    }
}

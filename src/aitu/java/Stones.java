package aitu.java;

public class Stones {
    private int id;
    private String name;
    private double price_per_carat;
    private double weight;
    private String type;
    Stones(int id, String name, double price_per_carat, double weight, String type){
        this.id = id;
        this.name = name;
        this.price_per_carat = price_per_carat;
        this.weight = weight;
        this.type = type;
    }

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName(){
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public double getPrice_per_carat(){
        return this.price_per_carat;
    }
    public void setPrice_per_carat(int price_per_carat) {
        this.price_per_carat = price_per_carat;
    }

    public double getWeight() {
        return this.weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public String getType() { return this.type; }
}
